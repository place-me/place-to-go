package logging

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	log "github.com/sirupsen/logrus"
)

func New() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		// Inject request scoped logger into context
		requestLogger := log.WithFields(log.Fields{
			"method": utils.CopyString(ctx.Method()),
			"path":   utils.CopyString(ctx.Path()),
		})
		ctx.Locals("logger", requestLogger)
		return ctx.Next()
	}
}
