package context

import (
	"context"
	"fmt"
	"log"
	"net"
	"testing"

	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"

	"gitlab.com/place-me/place-to-go/grpc/internal/helloworld"
	"google.golang.org/grpc"

	"google.golang.org/grpc/test/bufconn"
)

const TENANT = "65eeaf07-a308-4663-9a1b-abbed7867a62"
const USER = "30cecb4c-b845-4d79-9503-6b2b8e8746a8"
const bufSize = 1024 * 1024

var lis *bufconn.Listener

// server is used to implement helloworld.GreeterServer.
type TestServer struct {
	helloworld.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *TestServer) SayHello(ctx context.Context, in *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	log.Printf("Received: %v", in.GetName())
	tenantId := ExtractTenantId(ctx)
	userId := ExtractUserId(ctx)

	return &helloworld.HelloReply{Message: fmt.Sprintf("%s,%s", tenantId, userId)}, nil
}

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer(grpc.UnaryInterceptor(grpc_auth.UnaryServerInterceptor(CheckForPlacemeContext)))
	helloworld.RegisterGreeterServer(s, &TestServer{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func Test_MetadataCanBeExtractedFromPlacemeContext(t *testing.T) {
	ctx := context.Background()
	ctx = WithPlacemeContext(ctx, USER, TENANT)

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := helloworld.NewGreeterClient(conn)
	resp, err := client.SayHello(ctx, &helloworld.HelloRequest{
		Name: "Dr. Doom",
	})
	if err != nil {
		t.Fatalf("SayHello failed: %v", err)
	}

	if resp.Message != fmt.Sprintf("%s,%s", TENANT, USER) {
		t.Fatalf("SayHello failed: %v", err)
	}
}
