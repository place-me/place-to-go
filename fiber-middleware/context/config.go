package context

import (
	"github.com/gofiber/fiber/v2"
)

// Config defines the config for middleware.
type Config struct {
	// Filter defines a function to skip middleware.
	// Optional. Default: nil
	Filter func(*fiber.Ctx) bool

	// Required defines a function to enforce the middleware for certain cases.
	// Optional. Default: nil
	Required func(*fiber.Ctx) bool

	// Name of the header which contains placeme context informations
	// Optional. Default: Placeme-Context
	HeaderName string
}

// ConfigDefault is the default config
var ConfigDefault = Config{
	Filter:     nil,
	HeaderName: "Placeme-Context",
}

// Helper function to set default values
func configDefault(config ...Config) Config {
	// Return default config if nothing provided
	if len(config) < 1 {
		return ConfigDefault
	}

	// Override default config
	cfg := config[0]

	// Set default values
	if len(cfg.HeaderName) <= 0 {
		cfg.HeaderName = ConfigDefault.HeaderName
	}
	return cfg
}
