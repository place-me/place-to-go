package dapr

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type DaprInterceptor struct {
	DaprAppId string
}

func (di *DaprInterceptor) intercept(ctx context.Context, method string, req interface{},
	reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	ctx = metadata.AppendToOutgoingContext(ctx, "dapr-app-id", di.DaprAppId)
	return invoker(ctx, method, req, reply, cc, opts...)
}

func (di *DaprInterceptor) GetDialOption() grpc.DialOption {
	return grpc.WithUnaryInterceptor(di.intercept)
}
