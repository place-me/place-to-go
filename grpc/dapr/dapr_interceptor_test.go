package dapr

import (
	"context"
	"log"
	"net"
	"testing"

	"gitlab.com/place-me/place-to-go/grpc/internal/helloworld"
	"google.golang.org/grpc"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

// server is used to implement helloworld.GreeterServer.
type TestServer struct {
	helloworld.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *TestServer) SayHello(ctx context.Context, in *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	md, exists := metadata.FromIncomingContext(ctx)
	if exists == false {
		return nil, status.Error(codes.InvalidArgument, "No metadata available in incoming context on server")
	}

	daprAppIdValues := md.Get("dapr-app-id")
	if len(daprAppIdValues) != 1 {
		return nil, status.Error(codes.InvalidArgument, "Wrong number of values for metadata key 'dapr-app-id'")
	}

	daprAppId := daprAppIdValues[0]
	if len(daprAppId) <= 0 {
		return nil, status.Error(codes.InvalidArgument, "'dapr-app-id' is empty")
	}

	return &helloworld.HelloReply{Message: daprAppId}, nil
}

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	helloworld.RegisterGreeterServer(s, &TestServer{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func Test_DaprAppIdIsAddedToOutgoingContext(t *testing.T) {
	ctx := context.Background()

	daprInterceptor := &DaprInterceptor{
		DaprAppId: "get-beer-svc",
	}

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure(), daprInterceptor.GetDialOption())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := helloworld.NewGreeterClient(conn)
	resp, err := client.SayHello(ctx, &helloworld.HelloRequest{
		Name: "Beer time",
	})

	if err != nil {
		t.Fatalf("SayHello failed: %v", err)
	}

	if resp.Message != "get-beer-svc" {
		t.Fatal("'dapr-app-id' was not correctly transmitted to server")
	}
}
