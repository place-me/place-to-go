package idp

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

type Config struct {
	BindAddr string
	Issuer   string
}

type OidcProvider struct {
	cfg Config
	n   string
	e   string
	key *rsa.PrivateKey
}

func (oidc *OidcProvider) GenerateBearerToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	tokenSigned, err := token.SignedString(oidc.key)
	if err != nil {
		return "", fmt.Errorf("error while signing the token: %w", err)
	}

	return tokenSigned, nil
}

// Starts a mux which handles openid discovery
func (oidc *OidcProvider) CreateServer() *fiber.App {
	// Bootstrap fiber app
	app := fiber.New()
	app.Get("/keys", oidc.keysEP)
	app.Get("/.well-known/openid-configuration", oidc.discoveryEP)

	return app
}

// Creates a new instance of the oidc-provider
func New(cfg Config) (*OidcProvider, error) {
	// Create keys
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, fmt.Errorf("generating key for signing: %w", err)
	}

	n := base64.RawURLEncoding.EncodeToString(key.N.Bytes())
	eBin := make([]byte, 8)
	binary.BigEndian.PutUint64(eBin, uint64(key.E))
	eBin = bytes.TrimLeft(eBin, "\x00")
	e := base64.RawURLEncoding.EncodeToString(eBin)

	// Create provider
	oidc := &OidcProvider{
		cfg: cfg,
		n:   n,
		e:   e,
		key: key,
	}
	return oidc, nil
}
