package tracing

import (
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	log "github.com/sirupsen/logrus"
	"github.com/uber/jaeger-client-go"
	jaegerConfig "github.com/uber/jaeger-client-go/config"
)

const uberTraceIdHeaderKey = "uber-trace-id"

type Config struct {
	// Filter defines a function to skip middleware.
	// Optional. Default: nil
	Filter func(*fiber.Ctx) bool

	// Service name
	// Required if not set via env variables
	ServiceName string
}

func New(config ...Config) fiber.Handler {

	defcfg := jaegerConfig.Configuration{
		Sampler: &jaegerConfig.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegerConfig.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
		},
	}
	var cfg Config
	if len(config) > 0 {
		cfg = config[0]
	}

	if cfg.ServiceName != "" {
		defcfg.ServiceName = cfg.ServiceName
	}

	jaegerCfg, err := defcfg.FromEnv()
	if err != nil {
		log.Fatal("Could not parse Jaeger env vars: " + err.Error())
	}

	tracer, _, err := jaegerCfg.NewTracer()
	if err != nil {
		log.Fatal("Could not initialize jaeger tracer: " + err.Error())
	}

	return func(ctx *fiber.Ctx) error {
		// Filter request to skip middleware
		if cfg.Filter != nil && cfg.Filter(ctx) {
			return ctx.Next()
		}

		var logger *log.Entry

		if ctx.Locals("logger") == nil {
			logger = log.NewEntry(log.New())
		} else {
			logger = ctx.Locals("logger").(*log.Entry)
		}

		uberTraceIdHeader := ctx.Get(uberTraceIdHeaderKey)
		if uberTraceIdHeader == "" {
			logger.Debug("No uber trace id found in headers, request will not be traced")
			return ctx.Next()
		}

		operationName := "HTTP " + ctx.Method() + " URL: " + ctx.Path()
		hdr := make(http.Header)

		ctx.Request().Header.VisitAll(func(k, v []byte) {
			hdr.Set(utils.CopyString(utils.UnsafeString(k)), utils.CopyString(utils.UnsafeString(v)))
		})

		spanCtx, err := jaeger.ContextFromString(uberTraceIdHeader)
		if err != nil {
			logger.Error(err.Error())
			return ctx.Next()
		}

		span := tracer.StartSpan(operationName, opentracing.ChildOf(spanCtx))
		span.SetTag("http.remote_addr", ctx.IP())
		span.SetTag("http.path", ctx.Path())
		span.SetTag("http.host", ctx.Hostname())
		span.SetTag("http.method", ctx.Method())
		span.SetTag("http.url", ctx.OriginalURL())

		defer func() {
			status := ctx.Response().StatusCode()

			ext.HTTPStatusCode.Set(span, uint16(status))

			if status >= fiber.StatusInternalServerError {
				ext.Error.Set(span, true)
			}

			span.Finish()
		}()

		return ctx.Next()
	}
}
