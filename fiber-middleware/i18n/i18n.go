package i18n

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func New() fiber.Handler {

	matcher := language.NewMatcher(message.DefaultCatalog.Languages())

	return func(ctx *fiber.Ctx) error {

		// Order of language detection and fallback
		var langTag = utils.CopyString(ctx.Query("lang"))
		if len(langTag) < 1 {
			acceptedLanguages := strings.Split(utils.CopyString(ctx.Get("Accept-Language")), ",")
			if len(acceptedLanguages) > 0 && acceptedLanguages[0] != "" {
				langTag = acceptedLanguages[0]
			}
		}
		if len(langTag) < 1 {
			langTag = language.English.String()
		}

		lang, err := language.Parse(langTag)
		if err != nil {
			lang = language.English
		}

		tag, _, _ := matcher.Match(lang)
		p := message.NewPrinter(tag)
		ctx.Locals("printer", p)

		return ctx.Next()
	}
}
