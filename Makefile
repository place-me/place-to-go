# Choose the target language.
LANGUAGE ?= go

INCLUDES+=-I .
INCLUDES+=-I ./grpc/common
INCLUDES+=-I ./grpc/internal/helloworld

PATHS ?= paths=source_relative
# Choose the output directory
OUTPUT ?= .

FLAGS+= --$(LANGUAGE)_out=$(OUTPUT)
FLAGS+= --go_opt=$(PATHS)
FLAGS+= --$(LANGUAGE)-grpc_out=$(OUTPUT)
FLAGS+= --go-grpc_opt=$(PATHS)
FLAGS+= --experimental_allow_proto3_optional=true
FLAGS+= --validate_out=lang=$(LANGUAGE),$(PATHS):$(OUTPUT)

SUFFIX:= pb.go

DATE ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat .version 2> /dev/null || echo v0)

GOMODULES:= $(dir $(shell find . -name go.mod))

PKGS = $(or $(PKG),$(foreach dir, $(GOMODULES), $(shell cd $(dir) && $(GO) list ./...)))
BIN = bin
GO = go
TIMEOUT = 15
PROTOC ?= protoc

DOCKER_REGISTRY ?= #if set it should finished by /
EXPORT_RESULT ?= false # for CI please set EXPORT_RESULT to true

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell if [ "$$(tput colors 2> /dev/null || echo 0)" -ge 8 ]; then printf "\033[34;1m▶\033[0m"; else printf "▶"; fi)

.PHONY: all
all: help

.PHONY: all
all: help

tmp:
	echo "$(PKGS)"

# Tools

$(BIN):
	@mkdir -p $@
$(BIN)/%: | $(BIN) ; $(info $(M) building $(PACKAGE)…)
	$Q env GOBIN=$(abspath $(BIN)) $(GO) install $(PACKAGE)

PROTOCGENGO = $(BIN)/protoc-gen-go
$(BIN)/protoc-gen-go: PACKAGE=google.golang.org/protobuf/cmd/protoc-gen-go@latest

PROTOCVALIDATE = $(BIN)/protoc-gen-validate
$(BIN)/protoc-gen-validate: PACKAGE=github.com/envoyproxy/protoc-gen-validate@latest

PROTOCGRPC = $(BIN)/protoc-gen-go-grpc
$(BIN)/protoc-gen-go-grpc: PACKAGE=google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

GOCOV = $(BIN)/gocov
$(BIN)/gocov: PACKAGE=github.com/axw/gocov/gocov@latest

GOCOVXML = $(BIN)/gocov-xml
$(BIN)/gocov-xml: PACKAGE=github.com/AlekSi/gocov-xml@latest

GOTESTSUM = $(BIN)/gotestsum
$(BIN)/gotestsum: PACKAGE=gotest.tools/gotestsum@latest

PLUGINS+= --plugin=$(abspath $(PROTOCGENGO))
PLUGINS+= --plugin=$(abspath $(PROTOCVALIDATE))
PLUGINS+= --plugin=$(abspath $(PROTOCGRPC))

PROTOS:= $(shell find grpc -type f -name '*.proto' | sed "s/proto$$/$(SUFFIX)/")

%.$(SUFFIX): %.proto | $(PROTOCGENGO) $(PROTOCVALIDATE) $(PROTOCGRPC)
	$(PROTOC) $(PLUGINS) $(INCLUDES) $(FLAGS) $<

## Build:
protos: $(PROTOS) ; $(info $(M) genrating go proto buffers ...) @ ## Build go proto buffers


## Tests:
.PHONY: test

# Tests

TEST_TARGETS := test-short test-verbose test-race
.PHONY: $(TEST_TARGETS) check test tests

test-short:   ARGS=-short        ## Run only short tests
test-verbose: ARGS=-v            ## Run tests in verbose mode with coverage reporting
test-race:    ARGS=-race         ## Run tests with race detector

$(TEST_TARGETS): NAME=$(MAKECMDGOALS:test-%=%)
$(TEST_TARGETS): test
check test tests: | $(GOTESTSUM) ; $(info $(M) running $(NAME:%=% )tests…) @ ## Run tests
	$Q mkdir -p test
	$Q $(GOTESTSUM) --junitfile test/tests.xml -- -timeout $(TIMEOUT)s $(ARGS) $(PKGS)

.PHONY: test-bench
test-bench: $(GENERATED) ; $(info $(M) running benchmarks…) @ ## Run benchmarks
	$Q $(GOTESTSUM) -f standard-quiet -- --timeout $(TIMEOUT)s -run=__absolutelynothing__ -bench=. $(PKGS)

COVERAGE_MODE = atomic
.PHONY: test-coverage
test-coverage: fmt lint $(GENERATED)
test-coverage: | $(GOCOV) $(GOCOVXML) $(GOTESTSUM) ; $(info $(M) running coverage tests…) @ ## Run coverage tests
	$Q mkdir -p test
	$Q $(GOTESTSUM) -- \
		-coverpkg=$(shell echo $(PKGS) | tr ' ' ',') \
		-covermode=$(COVERAGE_MODE) \
		-coverprofile=test/profile.out $(PKGS)
	$Q $(GO) tool cover -html=test/profile.out -o test/coverage.html
	$Q $(GOCOV) convert test/profile.out | $(GOCOVXML) > test/coverage.xml
	@echo -n "Code coverage: "; \
		echo "scale=1;$$(sed -En 's/^<coverage line-rate="([0-9.]+)".*/\1/p' test/coverage.xml) * 100 / 1" | bc -q

.PHONY: clean
clean: ; $(info $(M) cleaning…)	@ ## Cleanup everything
	rm -rf $(BIN)

## Help:
.PHONY: help
help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)

.PHONY: version
version:
	@echo $(VERSION)
