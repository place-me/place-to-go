package observability

import (
	"fmt"

	jaegercfg "github.com/uber/jaeger-client-go/config"

	"github.com/opentracing/opentracing-go"
)

func SetupTracing(serviceName string) error {
	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		return fmt.Errorf("creating Jaeger config from environment: %w", err)
	}

	if cfg.ServiceName == "" {
		cfg.ServiceName = serviceName
	}

	tracer, _, err := cfg.NewTracer() // we never close the tracer
	if err != nil {
		return fmt.Errorf("creating Jaeger tracer: %w", err)
	}

	opentracing.SetGlobalTracer(tracer)

	return nil
}
