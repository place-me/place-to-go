# place-to-go

This is a lib containing multiple go modules which provide shared functionality used by place.me.

The following packages are available:

## gitlab.com/place-me/place-to-go/fiber-middleware

Provides middlewares which can be used in go applications using fiber.
## gitlab.com/place-me/place-to-go/grpc

Provides middlewares which can be used in go applications using gRPC.

## gitlab.com/place-me/place-to-go/package-renamer

Provides an utility to rename packages within a go file. This is required by gotext translations within several clients.