package idp

import (
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/place-me/place-to-go/fiber-middleware/auth"
)

func Test_CanCreateBearerToken(t *testing.T) {
	cfg := Config{
		BindAddr: "0.0.0.0:8080",
		Issuer:   "https://api.gateway.placeme.test",
	}

	idp, err := New(cfg)

	if err != nil {
		t.Error("unable to instantiate idp")
	}

	claims := auth.PlacemeClaims{
		Extensions: auth.PlacemeExt{
			TenantId:    "some-tenant-id",
			Userprofile: "some-userprofile-id",
		},
		RegisteredClaims: jwt.RegisteredClaims{
			Audience: []string{"https://api.gateway.placeme.ch"},
			Subject:  "this is the user",
			ExpiresAt: &jwt.NumericDate{
				Time: time.Now().Add(8 * time.Hour),
			},
			Issuer: cfg.Issuer,
			IssuedAt: &jwt.NumericDate{
				Time: time.Now(),
			},
		},
	}

	bearer, err := idp.GenerateBearerToken(claims)
	if err != nil {
		t.Error("unable to create bearer token")
	}

	if len(bearer) <= 0 {
		t.Errorf("invalid bearer token")
	}
}
