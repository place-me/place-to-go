package auth

import (
	"fmt"
	"log"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/golang-jwt/jwt/v4"
	idp "gitlab.com/place-me/place-to-go/fiber-middleware/auth/internal"
)

type PlacemeClaimsSingleAudience struct {
	Extensions PlacemeExt `json:"ext"`
	jwt.StandardClaims
}

var cfg idp.Config
var provider *idp.OidcProvider

func init() {
	cfg = idp.Config{
		BindAddr: "127.0.0.1:8080",
		Issuer:   "http://127.0.0.1:8080",
	}

	idp, err := idp.New(cfg)
	if err != nil {
		log.Fatal(err)
	}
	provider = idp
	idpServer := idp.CreateServer()
	go idpServer.Listen(cfg.BindAddr)

	// HACK: Give the webserver some time to startup
	time.Sleep(300 * time.Millisecond)
}

func TestAuthenticationSingleAudience(t *testing.T) {

	pctx := PlacemeExt{
		TenantId:    "ice ce tenant",
		Userprofile: "userprofile biatch",
	}

	claims := PlacemeClaimsSingleAudience{
		Extensions: pctx,
		StandardClaims: jwt.StandardClaims{
			Audience:  "e2e test",
			Subject:   "Unittester",
			ExpiresAt: time.Now().Add(8 * time.Hour).Unix(),
			Issuer:    cfg.Issuer,
			IssuedAt:  time.Now().Unix(),
		},
	}

	bearer, err := provider.GenerateBearerToken(claims)
	if err != nil {
		t.Fatal(err)
	}

	app := fiber.New()
	app.Use(New(cfg.Issuer))

	app.Get("/", func(c *fiber.Ctx) error {
		ctx := c.Locals("placeme-context").(*PlacemeExt)
		utils.AssertEqual(t, pctx.Userprofile, ctx.Userprofile, "Userprofile")
		utils.AssertEqual(t, pctx.TenantId, ctx.TenantId, "TenantId")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Authorization", fmt.Sprintf("bearer %s", bearer))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func TestAuthenticationMultipleAudiences(t *testing.T) {

	pctx := PlacemeExt{
		TenantId:    "ice ce tenant",
		Userprofile: "userprofile biatch",
	}

	claims := PlacemeClaims{
		Extensions: pctx,
		RegisteredClaims: jwt.RegisteredClaims{
			Audience: []string{"e2e test audience 1", "e2e test audience 2"},
			Subject:  "Unittester",
			ExpiresAt: &jwt.NumericDate{
				Time: time.Now().Add(8 * time.Hour),
			},
			Issuer: cfg.Issuer,
			IssuedAt: &jwt.NumericDate{
				Time: time.Now(),
			},
		},
	}
	bearer, err := provider.GenerateBearerToken(claims)
	if err != nil {
		t.Fatal(err)
	}

	app := fiber.New()
	app.Use(New(cfg.Issuer))

	app.Get("/", func(c *fiber.Ctx) error {
		ctx := c.Locals("placeme-context").(*PlacemeExt)
		utils.AssertEqual(t, pctx.Userprofile, ctx.Userprofile, "Userprofile")
		utils.AssertEqual(t, pctx.TenantId, ctx.TenantId, "TenantId")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Authorization", fmt.Sprintf("bearer %s", bearer))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}
