package proxy

import (
	"github.com/gofiber/fiber/v2"
)

func New() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		protoHeader := ctx.Get("X-Forwarded-Proto", "")
		if protoHeader != "" {
			ctx.Context().Request.URI().SetScheme(protoHeader)
		}

		return ctx.Next()
	}
}
