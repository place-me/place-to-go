package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	filePtr := flag.String("file", "", "File whose package name should be updated. (Required)")
	newNamePtr := flag.String("new", "", "Old package name which should overridden. (Required)")
	oldNamePtr := flag.String("old", "", "New package name which should be used for the provided file. (Required)")
	dryRunPtr := flag.Bool("dryrun", false, "Indicates if a dry run should be executed which will not actually change any files. (Optional)")
	flag.Parse()

	if *filePtr == "" || *newNamePtr == "" || *oldNamePtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	fset := token.NewFileSet()
	goFile, err := parser.ParseFile(fset, *filePtr, nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	packagePosition := fset.Position(goFile.Package)
	pr := &PackageRenamer{
		packagePosition.Line,
		*oldNamePtr,
		*newNamePtr,
		fset,
		goFile,
	}
	ast.Walk(pr, goFile)

	var buf bytes.Buffer
	printer.Fprint(&buf, fset, goFile)
	if *dryRunPtr == true {
		fmt.Printf(buf.String())
		os.Exit(0)
	}

	ioutil.WriteFile(*filePtr, buf.Bytes(), 0644)
}

type PackageRenamer struct {
	packageIdentifierLine int
	oldName               string
	newName               string
	fset                  *token.FileSet
	file                  *ast.File
}

func (pr *PackageRenamer) Visit(node ast.Node) (w ast.Visitor) {
	switch n := node.(type) {
	case *ast.Ident:
		// Ignore function identifiers, we are only interested in the package
		if n.Obj != nil && n.Obj.Kind == ast.Fun {
			return pr
		}

		// Cross reference position lines and return if they do not match
		position := pr.fset.Position(pr.file.Package)
		if position.Line != pr.packageIdentifierLine {
			return pr
		}

		if n.Name == pr.oldName {
			n.Name = pr.newName
		}
	}

	return pr
}
