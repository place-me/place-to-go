package auth

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

type Authenticator struct {
	Issuer string

	verifier *oidc.IDTokenVerifier
}

type PlacemeExt struct {
	TenantId    string `json:"tenant"`
	Userprofile string `json:"userprofile"`
}

type PlacemeClaims struct {
	Extensions PlacemeExt `json:"ext"`
	jwt.RegisteredClaims
}

func (a *Authenticator) createProviderIfNotExists(ctx context.Context) error {
	if a.verifier != nil {
		return nil
	}

	provider, err := oidc.NewProvider(ctx, a.Issuer)
	if err != nil {
		return fmt.Errorf("creating OIDC provider: %w", err)
	}

	verifier := provider.Verifier(&oidc.Config{SkipClientIDCheck: true})
	a.verifier = verifier

	return nil
}

func New(issuer string) fiber.Handler {
	a := &Authenticator{
		Issuer: issuer,
	}
	// Try to create the OIDC provider. If the IdP is currently down,
	// we don't care about it and just log a message
	err := a.createProviderIfNotExists(context.Background())
	if err != nil {
		log.Println(err)
	}

	return func(ctx *fiber.Ctx) error {
		placemeCtx, err := a.Authenticate(ctx)
		if err != nil {
			return fmt.Errorf("authentication failed: %w", err)
		}

		ctx.Locals("placeme-context", placemeCtx)
		return ctx.Next()
	}
}

func (a *Authenticator) Authenticate(ctx *fiber.Ctx) (*PlacemeExt, error) {
	val := ctx.Get("Authorization")
	if val == "" {
		return nil, fmt.Errorf("unable to extract bearer token from authorizatino header")
	}

	// Split token and check the type of it
	splits := strings.SplitN(val, " ", 2)
	if len(splits) < 2 {
		return nil, fmt.Errorf("authorization header needs to be of type bearer")
	}

	if strings.ToLower(splits[0]) != "bearer" {
		return nil, fmt.Errorf("authorization header needs to be of type bearer")
	}

	bearer := splits[1]

	// Parse token
	username, tenantId, err := a.VerifyToken(ctx, bearer)
	if err != nil {
		log.Println(err)
		return nil, fmt.Errorf("authorization token invalid")
	}

	return &PlacemeExt{Userprofile: username, TenantId: tenantId}, nil
}

func (a *Authenticator) VerifyToken(ctx *fiber.Ctx, idToken string) (string, string, error) {
	token, err := a.verifier.Verify(ctx.Context(), idToken)
	if err != nil {
		return "", "", fmt.Errorf("validating access token: %w", err)
	}

	claims := &PlacemeClaims{
		Extensions: PlacemeExt{},
	}

	if err := token.Claims(claims); err != nil {
		return "", "", fmt.Errorf("extracting claims from token: %w", err)
	}

	if claims.Extensions.Userprofile == "" {
		return "", "", fmt.Errorf("userprofile claim is empty or not existent: %w", err)
	}

	if claims.Extensions.TenantId == "" {
		return "", "", fmt.Errorf("tenantId claim is empty or not existent: %w", err)
	}

	return claims.Extensions.Userprofile, claims.Extensions.TenantId, nil
}
