package context

import (
	"context"
	"fmt"
	"strings"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type contextKey int

const (
	UserProfileId contextKey = iota
	TenantId      contextKey = iota
)

func ExtractUserId(ctx context.Context) string {
	userProfileId, ok := ctx.Value(UserProfileId).(string)
	if !ok {
		return ""
	}

	return userProfileId
}

func ExtractTenantId(ctx context.Context) string {
	tenantId, ok := ctx.Value(TenantId).(string)
	if !ok {
		return ""
	}

	return tenantId
}

// CheckForPlacemeContext is used by a middleware to authenticate requests
func CheckForPlacemeContext(ctx context.Context) (context.Context, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.Unauthenticated, "No metadata found")
	}

	headers := md.Get("placeme-context")
	if len(headers) <= 0 {
		return nil, status.Errorf(codes.Unauthenticated, "No placeme context found")
	}

	placemeCtx := headers[0]

	contextParts := strings.Split(placemeCtx, ",")
	splitsCount := len(contextParts)
	if splitsCount <= 0 {
		return nil, status.Errorf(codes.Unauthenticated, "Malformed placeme-context header")
	}
	newCtx := context.WithValue(ctx, UserProfileId, contextParts[0])
	if splitsCount > 1 {
		newCtx = context.WithValue(newCtx, TenantId, contextParts[1])
	}

	return newCtx, nil
}

func WithPlacemeContext(ctx context.Context, user string, tenant string) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "placeme-context", fmt.Sprintf("%s,%s", user, tenant))
}
